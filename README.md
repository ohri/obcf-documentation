---
Ottawa Bioinformatics Core Facility Documentation
---

The Ottawa Bioinformatics Core Facility is a Core Facity of the [University of Ottawa](https://med.uottawa.ca/core-facilities/facilities/bioinformatics) and the [Ottawa Hospital Research Institute](http://www.ohri.ca/bioinformatics/)

This is a repository of public facing documents which may be linked from other sites.

**Table of Contents** 

- [Sequencing Recommendations](https://gitlab.com/ohri/obcf-documentation/-/blob/master/Files/OBCF_sequencing_recommendations_March_2022.pdf)
- [HOWTO run nf-core/rnaseq on the graham cluster](Files/nfcore-rnaseq-graham-HOWTO.md)
- [nfcore config file for graham cluster](Files/obcf-graham.cfg)



