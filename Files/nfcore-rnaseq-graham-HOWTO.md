# How to run nf-core RNA-seq analysis on the DRAC graham cluster

_UPDATED June 13 2023 to account for changes in the graham cluster config_

This document is maintained by the [Ottawa Bioinformatics Core Facility](https://med.uottawa.ca/core-facilities/facilities/bioinformatics). Please [Contact Us](mailto:gpalidwor@ohri.ca) if you have any questions or feedback on this or other documents.

## Prerequisites
* You should have a [DRAC account](https://alliancecan.ca/en/services/advanced-research-computing/account-management/user-roles-access-resources-and-services-federation) and access to the DRAC graham cluster
* You should be able to log into the graham cluster using [ssh](https://docs.alliancecan.ca/wiki/SSH)  and [transfer data to and from graham](https://docs.alliancecan.ca/wiki/Transferring_data) .
* You should have basic Unix command line skills; an understanding of directory structures 
* Sequencing data to be analyzed in the form of FASTQ files

## Introduction  
[nf-core](https://nf-co.re/pipelines) provides a set of  bioinformatics pipelines for analysis of sequencing data. This document covers running the [rnaseq](nf-co.re/rnaseq) pipeline specifically on the graham compute cluster. This pipeline takes sequencing data in the form of FASTQ files, aligns it to the requested genome/transcriptome generating a quality report and gene expression estimation files of various types (counts, TPM). 

The Graham cluster does not allow data download from the cluster compute nodes which run the pipeline. This means we must pre-download the pipeline we are using so that it can be run in "offline" mode. There are three login nodes, `gra-login1` to `gra-login3`, you will be allocated to one randomly when you login to the graham cluster. As well, it is preferred that any large data transfers are not done on the login nodes, but rather should be done on a node specifically intended for that purpose, `gra-dtn1`. Regardless, data transfers from the internet are not possible from compute node so must be done on a login node (e.g. `gra-login1`) or `gra-dtn1`.

When using graham we recommend creating a [`tmux`](https://github.com/tmux/tmux/wiki) sessions on the login node. This allows your session to persist and running programs to continue if you are disconnected. To do this type `tmux` on the command line when you are logged in, and note the login node (`gra-login1`,`gra-login2`, etc). If disconnected, log back into graham. If you are not on the login node, `ssh` to that node, and type `tmux attach` to reconnect.


##  Setup
Create a working directory in a location in which you have space to store several gigabytes of data.  The exact size required depends on the amount sequencing data you have. You can determine your space allocations by typing `quota` on the command line.

## Downloading the pipeline 
The graham cluster allocates a large amount of non backed up space for every user in the path `/scratch/$USER` where `$USER` is your login userid. Type `echo $USERID` if you don't know what it is.

DRAC clusters including graham use the  [`lmod`](https://lmod.readthedocs.io/en/latest/) system to manage loading and unloading software modules. All the modules necessary for downloading and running `nf-core/rnaseq` can be loaded using the lmod system via the `module load` command.

To download the current RNA-seq pipeline we'll create a directory in `/scratch/$USER` and download it there. 

First, ssh to the `gra-dtn1` node, then do the following:

```bash
#create an nfcore directory
mkdir -p /scratch/$USER/nf-core
#cd to the new nfcore directory
cd /scratch/$USER/nf-core

#install the tools to download nf-core
module load python/3.11.5
module load  StdEnv/2023 postgresql/16.0
python  -m  venv  ENV
source  ENV/bin/activate
python  -m  pip  install nf-core
deactivate
```
Now the tools are installed we can use them to download the nf-core pipeline. This may take an hour or more.

Run the following to download the rna-seq pipeline. This will be the most current version, something like `nf-core-rnaseq-3.11.2`. 

```bash
module load StdEnv/2020
module load nextflow/22.10.6
module load apptainer/1.1
module load java/13.0.2
NXF_SINGULARITY_CACHEDIR=/scratch/${USER}/nf-core/
cd $NXF_SINGULARITY_CACHEDIR
source /scratch/$USER/nf-core/ENV/bin/activate
nf-core download rnaseq 
deactivate
```
You will be provided with some options during the download, choose as follow:
* _release branch_ : choose the most recent version. You must relace the version in the code below with the version you choose.
*  _Include the nf-core's default institutional configuration files into the download?_ : Yes
* _Download software container images_: Singularity
* _Copy singularity images from $NXF_SINGULARITY_CACHEDIR to the target folder or amend new images to the cache?_: amend
* _Choose compression type:_ none

When this is downloaded, check to see what the version of the pipeline is  by typing `ls`.

Now the pipeline is loaded and ready to run on the cluster!

##### Staging the FASTQ files
FASTQ files are the raw output of sequencers. You will need to move this sequencing data onto the graham cluster in order to analyze it.

* If you have already have FASTQ files you can [transfer them](https://docs.alliancecan.ca/wiki/Transferring_data) to the above-created directory. 
* If the files have been shared with you via BaseSpace facility such as [StemCore Laboratories](https://www.ohri.ca/stemcore/), you can download the FASTQ files from BaseSpace to your local machine, then transfer them to the graham cluster as above. There is also a command line tool `bs` ([documented here](https://developer.basespace.illumina.com/docs/content/documentation/cli/cli-overview)) which will transfer files shared on  basespace directly to the cluster. 

If this is experimental data from  your own lab it is recommended that you retain the original unmodified FASTQ files for future reference.

* If you are transferring data from SRA or ENA you will need to find the data source and download the data directly to your desktop and transfer it from there. For SRA this is generally found by looking at the referenced sequences in GEO,  linked at the bottom of the series and sample pages as `SRA Run Selector`. For ENA the downloading process is described [here](https://ena-docs.readthedocs.io/en/latest/retrieval/file-download.html) Therean online toolset available in the module loading system of graham called [`sra-toolkit`](https://github.com/ncbi/sra-tools) , which contains the [`fasterq-download`](https://github.com/ncbi/sra-tools/wiki/HowTo:-fasterq-dump) program which may be used to download SRA or ENA files directly to your working director in the form of FASTQ files by their identifier (e.g. `SRR000001`), for example the following two commands will load the `sra-tools` module and download the `SRR000001` files. The additional modules listed in the `module load` line, `StdEnv/2020` and `gcc/9.3.0` are other modules on which that sra-toolkit depends.
 
```bash
module load  StdEnv/2020  gcc/9.3.0  sra-toolkit
fasterq-dump SRR000001
```

* Note that `fasterq-dump` may download multiple fastq files, there will be two if the source sequencing was paired end.
* There is also a `nf-core/fetchngs` tool which should download the data but when I last tried to use it I got an error and have not followed it up; I will document that here when I do get it working


### nf-core rnaseq analysis configuration

The official `nf-core/rnaseq` usage documentation is [here](https://nf-co.re/rnaseq)

Once you have all of your fastq files downloaded into a working directory, you need to create a configuration file in the form of a samplesheet to tell the pipeline the relationship between the files and how to process them.

The samplesheet  can be named whatever you wish (usually `samplesheet.csv`) and is in comma separated format.  A template samplesheet can be automatically generated using a provided [fastq_dir_to_samplesheet.py](https://github.com/nf-core/rnaseq/blob/master/bin/fastq_dir_to_samplesheet.py) python script as described in the above linked usage documentation, with a command line like


You can download the `fastq_dir_to_samplesheet.py` python script with the following command line.

```bash
curl https://raw.githubusercontent.com/nf-core/rnaseq/master/bin/fastq_dir_to_samplesheet.py > fastq_dir_to_samplesheet.py
```

And run it with something like the following (depending on your sample types). Note that strandedness `auto` is safest as it will automatically determine whether the protocol used are unstranded or stranded and if stranded, which direction. This is particuarly helpful when you are analyzing FASTQ files from multiple experiments that may have varying protocols.

```bash
python fastq_dir_to_samplesheet.py --strandedness="auto"  --read1_extension="_1.fastq.gz" --read2_extension="_2.fastq.gz" `pwd` samplesheet.csv
```

You should choose sample names (see below) to be more human readable and informative as it will just default to the filenames as naming.

The samplesheet has four columns as named in the header and one file for each fastq file (if single end) or pair of fastq files (if paired end). 

```
sample,fastq_1,fastq_2,strandedness
```

* `sample` is the sample identifier. The same identifier should be used for every fastq file associated with a specific sample, so there can be multiple lines with the same name but different fastq file references. There may be multiple fastq files for a given sample e.g. when there are multiple lanes, files may be named like `590934_S28_L001_R1_001.fastq.gz` where `L001` indicates lane 1. The sample identifiers will be used in reports and tables generated by `nf-core/rnaseq` so it should be distinct and informative. 
* `fastq_1` is the filename of the read 1 fastq file. If the sample sequencing is single ended, that will be the only file and if paired end there will be a second file described by the next column. Read 1 files usually have `R1` in the filename as with the example above or  `_1.fastq.gz`  suffix.
* `fastq_2` is empty if the sample sequencing is single ended (so no text at all between the delimiting commans), and read 2 if it is paired ended; this will be a file of the same name as the R1 file with only read identifier differing.
* `strandedness` indicates the sequencing strand orientation. For Illumina and BGI this is usually `reverse`. If you're not sure, use `auto` and it will detect the strandedness. If you do use `auto`, then be sure to verify that it worked by reviewing the strandedness check section of the multiQC report.

If there are multiple sequencing runs of the same samples with the same Illumina run  configuration, as is sometimes performed by StemCore, the resulting filenames will be identical. They cannot be placed in the same directory as they will overwrite each other. In these cases you'll have to give the  path to the fastq files or (carefully!) rename the files in the same directory. It's preferable to keep them in separate directories so as not to modify the original data in any way.

Here is an example `samplesheet.csv` file for a single sample sequenced over 4 lanes  :

```
sample,fastq_1,fastq_2,strandedness
t0W,590908_S18_L001_R1_001.fastq.gz,,auto
t0W,590908_S18_L001_R1_001.fastq.gz,,auto
t0W,590908_S18_L002_R1_001.fastq.gz,,auto
t0W,590908_S18_L002_R1_001.fastq.gz,,auto
```
Once you've generated the sample sheet you're ready to run the analysis on the cluster

Note that though most recent Illumina RNA-seq experiments are negative stranded, you should verify by looking at the `multiQC` report `Infer experiment` output to ensure that the strand is set correctly.

### Allocating a SLURM session to run in

As many users are on the login node, we don't want to do substantial computation there so as not to overload it. We will use the cluster workload manager [slurm](https://slurm.schedmd.com/) to `nfcore/rnaseq`

When specifying slurm execution you must indicate various parameters including what grouop account this should be recorded under. Usually this will be your the group of the PI under whom you got the account, or a specfic reseach allocation. If you type `groups` you will see a list of unix groups you could potentially use. Please contact your PI or the OBCF if you have questions about which account is appropriate. In the examples below we are using `def-tperkins` as the account identifir.

Below we run `salloc` which will log you in to a cluster node with arguments specifying `32G` of memory, that you will have it for 24 hours and you will have 8 processors. Depending on the analysis you may need to specify a longer time, particularly if the slurm session times out before the analysis is complete. Be sure to log out of the node when the run is completed (`exit`).

```bash
salloc --account=def-tperkins --mem=32G --time=24:00:00 -c 8
```

This may take a while for the necessary resources to be allocated and login to occurr. Once logged in we execute the analysis on that node.

# Running your analysis

You should paste or type the following onto the command line

```bash
module load StdEnv/2020
module load nextflow/22.10.6
module load apptainer/1.1
module load java/13.0.2
```

Nextflow will run multiple slurm jobs to perform the analysis. To do this it must know the group to record the usage under. This is done by setting the appropriate group in enviornment variables, see below for an example using `def-tperkins`.

```bash
export SLURM_ACCOUNT=def-tperkins
export SALLOC_ACCOUNT=$SLURM_ACCOUNT
export SBATCH_ACCOUNT=$SLURM_ACCOUNT
```

As well we'll some variables in the environment

```bash
#ensure that the temporary directory used by nf-core is the one provided by the SLURM session
export TMPDIR=$SLURM_TMPDIR
#ensure that the session information is cached in your scratch directory 
NXF_SINGULARITY_CACHEDIR=/scratch/$USER/nf-core/
```

You can also add these to the `.bash_profile` and `.bashrc` files in your home directory and they will be added to your environment automatically on login.

Next we need to download the custom `nfcore/rnaseq` configuration which has been generate by the OBCF.  You can download it into your working directory from one of the login nodes using hte following command. Note that the cluster compute nodes cannot contact the internet.

```bash
curl https://gitlab.com/ohri/obcf-documentation/-/raw/master/Files/obcf-graham.cfg?inline=false > obcf-graham.cfg
```

Below it is assumed that:
* you are running in the same directory as the sample sheet.
* the sample sheet has been generated as described above and is named `sample_sheet.csv` 
* the `obcf_graham.cfg` file has been downloaded into the current directory as described above
* the `resultsdir` outputdir will be created in the same directory
* you are aligning to the `GRCh38` build of the human genome. We recommend using [GENCODE](https://www.gencodegenes.org/) annotations if aligning to mouse or human. For any alignment, all that is required is the `.gtf` and genomic fasta file for the organism.

We will download version 43 human GENCODE annotations to `/scratch/$USER/nf-core` and uncompress them:

```bash
cd /scratch/$USER/nf-core
wget https://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_43/GRCh38.primary_assembly.genome.fa.gz
gunzip GRCh38.primary_assembly.genome.fa.gz
wget https://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_43/gencode.v43.basic.annotation.gtf.gz
gunzip gencode.v43.basic.annotation.gtf.gz
```

Now that we've done all the necessary setup, we can run the `nf-core/rnaseq` analysis. Note that the below code assumes that the downloaded pipeline is `nf-core-rnaseq-3.11.2`. Check to see that this is the version you downloaded above. If not, replace with the version you downloaded in the script below.

First you should `cd` to the directory containing your previously created `samplesheet.csv`. It will output to the director `nfcore-results` in the same directory.

**PLEASE NOTE**: when executing the command line below, nextflow executes the `main.nf` file whose location can vary depending on the version of the pipeline being used. I the case below the `main.nf` location is `/scratch/$USER/nf-core/nf-core-rnaseq-3.11.2/main.nf`. You may need to check your downloaded version of the pipeline, then pass nextflow the appropriate directory in your execution command.

```bash
NXF_SINGULARITY_CACHEDIR=/scratch/$USER/nf-core/
nextflow run /scratch/$USER/nf-core/nf-core-rnaseq-3.11.2 \
	--input samplesheet.csv \
	--outdir nfcore-results \
	-c obcf-graham.cfg \
	--gtf $NXF_SINGULARITY_CACHEDIR/gencode.v43.basic.annotation.gtf \
	--fasta $NXF_SINGULARITY_CACHEDIR/GRCh38.primary_assembly.genome.fa \
	--gencode
```
This may take several hours depending on the number and size of fastq files being processed and how busy the cluster is. You will see details of the run status in the terminal window. When completed the terminal window will indicate this with run time and other statistics. It sometimes gets stuck there may not return to a command prompt. If it says it is completed you can press `ctrl-c` to terminate the run and type `exit` to leave the slurm `salloc` session.

### Accessing your Results Directory

The key items in the results directory are:

`multiqc/star_salmon/multiqc_report.html` which can be used to evaluate the sample quality, it is an integrated report for the pipeline analysis results

There are various outputs of the salmon pipeline [as described here](https://nf-co.re/rnaseq/3.10.1/output#salmon), it is recommended that either the salmon output is used directly with the R `tximport` function or `salmon.merged.gene_counts.tsv` though any may be used depending on your intended analysis.

The full directory is very large but we recommend keeping it all if possible. If not possible we recommend keeping at least the above documents as the multiqc file contains details of the analysis performed in sufficient detail for reproducibility and the generation of a Materials and Methods entry. And of course you should always retain your original fastq files. You may need to re-analyze them and if the results are used in a publication the data will likely need to be deposited in the NCBI GEO repository.


